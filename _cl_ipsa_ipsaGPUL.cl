//#include <pyopencl-ranluxcl.cl>

//[0,   0,    750,  30],  ## Rectum
//[200, 1500, 2250, 80],  ## Target
//[0,   0,    750,  90],  ## Bladder
//[30,  1400, 1600, 100], ## Urethra
//float16 SURF;// = (float16) (0, 0, 750, 30, 200, 1500, 2250, 80, 0, 0, 750, 90, 30, 1400, 1600, 100);
//[0, 0, 750, 30],  ## Rectum
//[200, 1500, 2250, 10],  ## Target
//[0,   0,    0,    0],   ## Bladder
//[30,  1400, 1600, 100], ## Urethra
//float16 VOLM;// = (float16) (0, 0, 750, 30, 200, 1500, 2250, 10, 0, 0, 000, 00, 30, 1400, 1600, 100);


/*Copy Data From Global to Local*/
__kernel void init_dose_calc (
	__global float* GDK, // Dose Kernels
	__constant int* GPO,   // Point Organs
	__constant int* GPT,   // Dose Types
    __global float* GDV,   // Dose Values
    __global float* GPV,   // Penalty Values
    __global float* GDT,   // Dwell Times
    __constant float* GSS,
    __constant float* GVV,
    int nBatch
) {


    //float SURF;
    //float VOLM;
 
    //// Parameters
    //int iGroup = get_group_id(0);
    //int iSize = get_group_size(0);
    //int iThread = get_local_id(0);
    int iGlobal = get_global_id(0);
    /*for (int iDT=0; iDT<145; iDT++) {//////////////////////////////////NEW LPL
        dk[92*iDT + iThread]=GDK[9118*iDT + iGroup*iThread];
       }
    */
    int iOrgan;
    int iType;
    float Penalty = 0;
    float Dose = 0;
    int iBase;

    if (iGlobal<9118) {
        float dt = 0;
        for (int iSA=0; iSA<nBatch; iSA++) {
            GDV[iSA*9118+iGlobal] = 0;// Reset Dose Vector

            //SURF = vload16(iSA, GSS);
            //VOLM = vload16(iSA, GVV);
            for (int iDT=0; iDT<145; iDT++) {
                dt = GDT[iSA*145+iDT];
                //GDV[iSA*9118+iGlobal] += dt * dk[92*iDT+iThread];
                GDV[iSA*9118+iGlobal] += dt * GDK[9118*iDT+iGlobal];
            }
            iOrgan = GPO[iGlobal];
            iBase  = 4*iOrgan;
            iType  = GPT[iGlobal];
            Dose = GDV[iSA*9118+iGlobal];
            Penalty = 0;
            if (iType==0) {
                if (Dose>GSS[16*iSA+iBase+2] && GSS[16*iSA+iBase+3]!=0) {
                    //Penalty = SURF[iBase+3] * ( Dose - SURF[iBase+2] );
                    Penalty = GSS[16*iSA+iBase+3] * ( Dose - GSS[16*iSA+iBase+2] );
                }
                else if (Dose<GSS[16*iSA+iBase+1] && GSS[16*iSA+iBase]!=0) {
                    //Penalty = -1 * SURF[iBase] * ( Dose - SURF[iBase+1] );
                    Penalty = -1 * GSS[16*iSA+iBase] * ( Dose - GSS[16*iSA+iBase+1] );
                }
            }
            else {
                if (Dose>GVV[16*iSA+iBase+2] && GVV[16*iSA+iBase+3]!=0) {
                    //Penalty = VOLM[iBase+3] * ( Dose - VOLM[iBase+2] );
                    Penalty = GVV[16*iSA+iBase+3] * ( Dose - GVV[16*iSA+iBase+2] );
                }
                else if (Dose<GVV[16*iSA+iBase+1] && GVV[16*iSA+iBase]!=0) {
                    Penalty = -1 * GVV[16*iSA+iBase] * ( Dose - GVV[16*iSA+iBase+1] );
                }
            }
            GPV[iSA*9118+iGlobal] = Penalty;
        }
    }
} // End-of-Kernel

__kernel void make_trial_change (
	__global float* GDK, // Dose Kernels 9118x145
	__constant int* GPO,   // Point Organs 9118x1
	__constant int* GPT,   // Dose Types 9118x1
    __global float* GDV,   // Dose Values 9118xKK
    __global float* GPV,   // Penalty Values 9118xKK
    __global int* iDTs,     //id deltaT 1xKK
    __global float* deltaTs, // delta T 1xKK
    __constant float* GSS, // Surface Potentiel
    __constant float* GVV, // Volume Potentiel
    int nBatch // KK
) {
    //float16 SURF;
    //float16 VOLM;
    //__local int idt [100];
    //__local float gss [16*100];
    //__local float gvv [16*100];
    //__local float deltaT [100];

    /*for (int i=0; i<100; i++) {
        idt[i] = iDTs[i];
        deltaT[i] = deltaTs[i];
    }*/
    /*for (int i=0; i<16*100; i++) {
        gss[i] = GSS[i];
        gvv[i] = GVV[i];
    }*/

    int iOrgan;
    int iType;
    float Penalty = 0.0;
    float Dose = 0.0;
    int iBase;
    //float deltaT = 0.0;

    int iGlobal = get_global_id(0);
    int iThread = get_local_id(0);

    if (iGlobal<9118) {
        for (int iSA=0; iSA<nBatch; iSA++) {
            //GDV[iSA*9118+iGlobal] += deltaT[iSA] * GDK[9118*idt[iSA]+iGlobal];
            GDV[iSA*9118+iGlobal] += deltaTs[iSA] * GDK[9118*iDTs[iSA]+iGlobal];    // <-- // ???
            //GDV[iSA*9118+iGlobal] += deltaTs[iSA] * GDK[iGlobal];

            iOrgan = GPO[iGlobal];
            iBase  = 4*iOrgan;
            iType  = GPT[iGlobal];
            Dose = GDV[iSA*9118+iGlobal];
            Penalty = 0.0;
            if (iType==0) {
                if (Dose>GSS[16*iSA+iBase+2] && GSS[16*iSA+iBase+3]!=0) {

                    Penalty = GSS[16*iSA+iBase+3] * ( Dose - GSS[16*iSA+iBase+2] );
                }
                else if (Dose<GSS[16*iSA+iBase+1] && GSS[16*iSA+iBase]!=0) {
                    //Penalty = -1 * SURF[iBase] * ( Dose - SURF[iBase+1] );
                    Penalty = -1 * GSS[16*iSA+iBase] * ( Dose - GSS[16*iSA+iBase+1] );
                }
            }
            else {
                if (Dose>GVV[16*iSA+iBase+2] && GVV[16*iSA+iBase+3]!=0) {

                    Penalty = GVV[16*iSA+iBase+3] * ( Dose - GVV[16*iSA+iBase+2] );
                }
                else if (Dose<GVV[16*iSA+iBase+1] && GVV[16*iSA+iBase]!=0) {
                    Penalty = -1 * GVV[16*iSA+iBase] * ( Dose - GVV[16*iSA+iBase+1] );
                }
            }
            /*if (iType==0) {
                if (Dose>gss[16*iSA+iBase+2] && gss[16*iSA+iBase+3]!=0) {

                    Penalty = gss[16*iSA+iBase+3] * ( Dose - gss[16*iSA+iBase+2] );
                }
                else if (Dose<gss[16*iSA+iBase+1] && gss[16*iSA+iBase]!=0) {
                    //Penalty = -1 * SURF[iBase] * ( Dose - SURF[iBase+1] );
                    Penalty = -1 * gss[16*iSA+iBase] * ( Dose - gss[16*iSA+iBase+1] );
                }
            }
            else {
                if (Dose>gvv[16*iSA+iBase+2] && gvv[16*iSA+iBase+3]!=0) {

                    Penalty = gvv[16*iSA+iBase+3] * ( Dose - gvv[16*iSA+iBase+2] );
                }
                else if (Dose<gvv[16*iSA+iBase+1] && gvv[16*iSA+iBase]!=0) {
                    Penalty = -1 * gvv[16*iSA+iBase] * ( Dose - gvv[16*iSA+iBase+1] );
                }
            }*/
            GPV[iSA*9118+iGlobal] = Penalty;

        }
    }

}


/*__kernel void cancel_trial_change (
	__global float* GDK, // Dose Kernels
	__constant int* GPO,   // Point Organs
	__constant int* GPT,   // Dose Types
    __global float* GDV,   // Dose Values
    __global float* GPV,   // Penalty Values
    __global float* GDT,   // Dwell Times
    __global int* iDTs,
    __global float* deltaTs,
    __constant float* GSS,
    __constant float* GVV,
    int nBatch
) {
    float16 SURF;
    float16 VOLM;

    int iOrgan;
    int iType;
    float Penalty = 0.0;
    float Dose = 0.0;
    int iBase;
    float deltaT;

    int iGlobal = get_global_id(0);

    if (iGlobal<9118) {
        for (int iSA=0; iSA<nBatch; iSA++) {
            SURF = vload16(iSA, GSS);
            VOLM = vload16(iSA, GVV);
            //deltaT = deltaTs[iSA];                  //Isn't it deltaTs[iSA] (was [iDTs[iSA]]?                    // <--
            //if (GDT[iSA*145+iDTs[iSA]]+deltaT<0.0) {                          // <--
            //    deltaT = 0. - GDT[iSA*145+iDTs[iSA]];                         // <--
            //}
            deltaT = deltaTs[iDTs[iSA]];                                       // <-- was deltaTs[iDTs[iSA]], mnop LPL
            if (GDT[iSA*145+iDTs[iSA]]-deltaT<0.0) {                          // <--
                deltaT = GDT[iSA*145+iDTs[iSA]];                              // <--
            }                                                                 // <--
            GDT[iSA*145+iDTs[iSA]]-= deltaT;                                  // <--
            GDV[iSA*9118+iGlobal] -= deltaT * GDK[9118*iDTs[iSA]+iGlobal];    // <--
            
            iOrgan = GPO[iGlobal];
            iBase  = 4*iOrgan;
            iType  = GPT[iGlobal];
            Dose = GDV[iSA*9118+iGlobal];
            if (iType==0) {
                if (Dose>SURF[iBase+2] && SURF[iBase+3]!=0) {
                    Penalty = SURF[iBase+3] * ( Dose - SURF[iBase+2] );
                } else if (Dose<SURF[iBase+1] && SURF[iBase]!=0) {
                    Penalty = -1 * SURF[iBase] * ( Dose - SURF[iBase+1] );
                }
            } else {
                if (Dose>VOLM[iBase+2] && VOLM[iBase+3]!=0) {
                    Penalty = VOLM[iBase+3] * ( Dose - VOLM[iBase+2] );
                } else if (Dose<VOLM[iBase+1] && VOLM[iBase]!=0) {
                    Penalty = -1 * VOLM[iBase] * ( Dose - VOLM[iBase+1] );
                }
            }
            GPV[iSA*9118+iGlobal] = Penalty;
        }
    }
}*/

