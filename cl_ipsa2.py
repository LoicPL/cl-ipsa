##############################################################
## python standard library
import numpy as np
import re
import sys
import random
import operator
import time

##############################################################
## PyOpenCL
import pyopencl as cl  # Import the OpenCL GPU computing API
import pyopencl.tools as pycl_tools
import pyopencl.array as pycl_array
from pyopencl.array import take
##############################################################
## globals


plat = cl.get_platforms()
DEV=plat[0].get_devices()
GPU=DEV[1]
MEM = GPU.get_info(cl.device_info.LOCAL_MEM_SIZE)

#Create context for GPU/CPU


context = cl.create_some_context()
queue = cl.CommandQueue(context)
#print(GPU.get_info(di.LOCAL_MEM_SIZE))
KK = 100 # <-- number of batches

#### Dose Kernels  ############################# FLOAT32
DKM=np.loadtxt("data/dose_kernel_matrix.txt", skiprows=1).astype(np.float32)
dk_shape = DKM.shape
DK=DKM.transpose().ravel()  ## now the data are like [points for 1st dwell; points for 2nd dwell; ...]
#### Dose Points  ############################# FLOAT32
DP=np.loadtxt("data/dose_kernel_points.txt", skiprows=0)
#### Point Infos  ############################# INT32
PO=DP[:,3].astype(np.int32)
PT=DP[:,4].astype(np.int32)
#### Dose Values Array ############################# FLOAT32
DV=np.zeros([PT.shape[0]*KK,], dtype="float32")
#### Penalty Values Array
PV=np.zeros([PT.shape[0]*KK,], dtype="float32")
#### Dwell Times
DT0=np.ones((dk_shape[1]*KK,), dtype="float32")

# print("Dose Kernel Matrix / Shape:", DK.shape, "<<==ravel(..)==", dk_shape)
# print("Dose Points Types  / Shape:", PT.shape)
# print("Dose Points Organs / Shape:", PO.shape)
# print("Dwell Times Initialized To 1.0s", DT.shape)
# print("Dose Points Values / Shape:", DV.shape)

def make_pots(KK):
    ss=[0, 0, 750, 30, 200, 1500, 2250, 80, 0, 0, 750, 90, 30, 1400, 1600, 100]*KK
    vv=[0, 0, 750, 30, 200, 1500, 2250, 10, 0, 0, 000, 00, 30, 1400, 1600, 100]*KK
    print("\nPotentials:")
    for kk in range(KK):
        print(">>> SURF",kk,ss[kk*16:(kk+1)*16])
    for kk in range(KK):
        print(">>> VOLM",kk,vv[kk*16:(kk+1)*16])
    return np.array(ss).astype("float32"), np.array(vv).astype("float32")

def dump_status(K_ITER, _T_, OBJ_BEST, SS):
    OBJ_BEST = OBJ_BEST.tolist()
    LEN = len(OBJ_BEST)
    print("CL-IPSA K_ITER=%06i, T=%0.2f, OBJs ="%(K_ITER, _T_), end=" ")
    if len(OBJ_BEST)>6:
        OBJ_BEST = OBJ_BEST[:5]+["...", OBJ_BEST[-1]]
    for IDX,OBJ in enumerate(OBJ_BEST):
        if IDX==len(OBJ_BEST)-1:
            IDX = LEN-1
        if type(OBJ)==type(""):
            print(" %s |"%(OBJ),end=" ")
        else:
            print("(%i) %.1f |"%(IDX+1, OBJ),end=" ")
    print(SS[0],SS[-1])



##############################################################
## the main app class
class App (object):

    def __init__(self): pass

    def calc_obj(self, pv, KK, XX):
        xx = [
            np.logical_and(PO==0, PT==0),
            np.logical_and(PO==0, PT==1),
            np.logical_and(PO==0, PT==2),
            np.logical_and(PO==1, PT==0),
            np.logical_and(PO==1, PT==1),
            np.logical_and(PO==1, PT==2),
            np.logical_and(PO==2, PT==0),
            np.logical_and(PO==2, PT==1),
            np.logical_and(PO==2, PT==2),
            np.logical_and(PO==3, PT==0),
            np.logical_and(PO==3, PT==1),
            np.logical_and(PO==3, PT==2)
            ]
        def calc(pvx,xx):
            xx=[pvx[x] for x in xx]
            xx = filter(lambda x:len(x)>0, xx)
            xx=[np.average(x) for x in xx]
            return np.sum(xx)
        RR = [calc(pv[I*XX:(I+1)*XX],xx) for I in range(KK)]
        return np.array(RR)





    def ipsa(self):
        """."""
        print("==================== IPSA")
        print(MEM)
        print(GPU.max_work_group_size)

        n_grp = 100
        n_pnt, n_dt = dk_shape
        wg_size = 92#int(np.ceil(n_pnt/float(n_grp)))
        local_size = (wg_size,)
        global_size = (local_size[0]*n_grp,)
        print("\nDevice Parameters")
        print(">>> %i global, %i local"%(global_size[0], local_size[0]))
        print(">>> %i dose points, %i dwells"%(n_pnt, n_dt))
        print(">>> %i work groups"%(n_grp))
        print(">>> each WG processes %i points"%(wg_size))
        #XX=np.zeros([n_dt, wg_size]).astype("float16")
        #YY=np.zeros([wg_size,]).astype("int8")
        POTS, POTV = make_pots(KK)
        #
        ##################
        ## SA Parameters
        Alpha  = 0.5
        Lambda = 2000#5000
        T_Init = 3762.0
        _T_    = T_Init          ## Temperature
        DT_StepSize_Max  = 1.0
        DT_StepSize_Min  = 0.1
        DT_StepSize_Redn = 0.1   ## DwellTime_StepSize_Reduction
        DT_InitValue     = 1.0
        DT_Min           = 0.0
        K_REFRESH        = 10  ## Output every 1000 iterations
        K_ITER = 0       ## Iteration Number
        K_MAXM = 5000   ## Max Iterations, K_Maximum
        K_REJT = np.array([0 for i in range(KK)])       ## K_Rejected
        K_REDN = 5                                      ## K_Reduction
        K_BEST = np.array([0 for i in range(KK)])       ## Iteration when Best ever Solution was found
        OBJ_BEST = np.array([1E+10 for i in range(KK)]) ## Iteration when Best ever Solution was found
        DT = DT0 * DT_InitValue

        DT_StepSize = np.array([DT_StepSize_Max for i in range(KK)]) ## StepSize
        #################
        with open("_cl_ipsa_ipsaGPUL.cl") as f: cl_code = f.read()
        # Setup Global Memory
        dk_buf = cl.array.to_device(queue, DK) # 9118x145 In/Dose Kernel Matrix in Global Memory
        po_buf = cl.array.to_device(queue, PO) # 9118x1   In/Dose Point Organs
        pt_buf = cl.array.to_device(queue, PT) # 9118x1   In/Dose Point Types
        dv_buf = cl.array.to_device(queue, DV) # 9118xKK  Out/Dose Values (Vector)
        pv_buf = cl.array.to_device(queue, PV) # 9118xKK  Out/Penalty Values (Vector)
        dt_buf = cl.array.to_device(queue, DT) # 145xKK   In/Out/Dwell Times
        ss_buf = cl.array.to_device(queue, POTS)  ## Surface Potentials
        vv_buf = cl.array.to_device(queue, POTV)  ## Volume Potentials
        ##################
        program = cl.Program(context, cl_code).build()
        init_dose_calc = program.init_dose_calc
        make_trial_change = program.make_trial_change
        #cancel_trial_change = program.cancel_trial_change
        # ##################
        print("\nIPSA STARTS\n==")
        ## Init Dose Calc
        K_ITER=0
        evt = init_dose_calc(queue, global_size, local_size,
            dk_buf.data, # Dose Kernel
            po_buf.data, # Point Organ
            pt_buf.data, # Point Type
            dv_buf.data, # Dose Values
            pv_buf.data, # Penalty Values
            dt_buf.data, # Dwell Times
            ss_buf.data, # SURF Potentials
            vv_buf.data, # VOLM Potentials
            np.int32(KK)
            )

        OBJ_CURR = self.calc_obj(pv_buf.get(), KK, 9118)
        OBJ_PREV = OBJ_CURR[:]
        OBJ_BEST = np.empty_like(OBJ_CURR)
        for i in range (KK):
            OBJ_BEST[i]=OBJ_CURR[i]
        DT_BEST = DT

        dump_status(K_ITER, _T_, OBJ_BEST, DT_StepSize)
        ##################
        ## Main SA LOOP
        dt_test = dt_buf.get()
        t0=time.clock()
        dk=np.zeros(9118*KK)
        while True:

            if K_ITER>K_MAXM: break

            ## Transition
            IDX=np.array([np.random.randint(0,144) for i in range(KK)]).astype("int32")
            idx_buf = cl.array.to_device(queue,
                IDX#was 145
                )


            ttt = np.array([
                np.random.choice([2, -2])*DT_StepSize[i] for i in range(KK)#goes faster when number between 1 and 10
                ]).astype("float32")
            deltaT = ttt[:]

            while (i < KK):
                ff=dt_test[i*145+IDX[i]]+deltaT[i]
                if ((dt_test[i*145+IDX[i]]+deltaT[i]) < 0.1):
                    deltaT[i] = 0. - dt_test[i*145+IDX[i]]
                dt_test[i*145+IDX[i]] += deltaT[i]
                i+=1
            ttt_buf = cl.array.to_device(queue, deltaT)

            make_trial_change(queue, global_size, local_size,
                dk_buf.data, # Dose Kernel
                po_buf.data, # Point Organ
                pt_buf.data, # Point Type
                dv_buf.data, # Dose Values
                pv_buf.data,  # Penalty Values
                idx_buf.data,# np.int32(30)
                ttt_buf.data,# np.float32(deltaT)
                ss_buf.data, # SURF Potentials
                vv_buf.data, # VOLM Potentials
                np.int32(KK)
            )
            #dt_test = dt_buf.get()
            ## Objectives
            OBJ_CURR = self.calc_obj(pv_buf.get(), KK, 9118) ## was map_to_host() change for get()
            ## Accept or not???

            #dt_now=dt_buf.get()
            PROB = np.exp( (OBJ_PREV - OBJ_CURR)/_T_ )
            rand = np.random.random((KK,))
            DLETA_T = np.empty_like(ttt)
            DLETA_T[:]=ttt
            for I in range(KK):
                if rand[I]>PROB[I]: ## Reject
                    OBJ_CURR[I] = OBJ_PREV[I]
                    K_REJT[I] += 1
                    DLETA_T[I] = -ttt[I]
                else:               ## Accept
                    OBJ_PREV[I] = OBJ_CURR[I]

                    K_REJT[I] = 0
                    DLETA_T[I] = 0.0
                    if OBJ_CURR[I]<OBJ_BEST[I]:
                        OBJ_BEST[I]=OBJ_CURR[I]
                        K_BEST[I]=K_ITER
                        for J in range (145):
                            DT_BEST[145*I+J]=dt_test[145*I+J]


            ##if K_ITER%K_REFRESH == 0: print(ttt)
            ttt_buf = cl.array.to_device(queue, ttt)
            ## Update Dose & Penalty
            i = 0
            while (i < KK):
                ff = dt_test[i * 145 + IDX[i]] + DLETA_T[i]
                if ((dt_test[i * 145 + IDX[i]] + DLETA_T[i]) < 0.1):
                    DLETA_T[i] = 0. - dt_test[i * 145 + IDX[i]]
                dt_test[i * 145 + IDX[i]] += DLETA_T[i]
                i += 1
            #dt_buf = cl.array.to_device(queue, dt_test)
            dtt_buf = cl.array.to_device(queue, DLETA_T)
            make_trial_change(queue, global_size, local_size,
                dk_buf.data, # Dose Kernel
                po_buf.data, # Point Organ
                pt_buf.data, # Point Type
                dv_buf.data, # Dose Values
                pv_buf.data, # Penalty Values
                idx_buf.data,# np.int32(30),
                dtt_buf.data, # np.float32(deltaT)
                ss_buf.data, # SURF Potentials
                vv_buf.data, # VOLM Potentials
                np.int32(KK)
            )
            OBJ_CURR = self.calc_obj(pv_buf.get(), KK, 9118)
            ## Update DT_StepSize / Step Size Reduction
            DT_StepSize = np.where(
                K_REJT>=K_REDN,
                np.where(
                    (DT_StepSize-DT_StepSize_Redn)<DT_StepSize_Min,
                    DT_StepSize_Min,
                    DT_StepSize-DT_StepSize_Redn
                    ),
                DT_StepSize
                )
            ## ITER
            K_ITER += 1
            ## Cooling Schedule -- Temperature
            _T_ = T_Init / pow(float(K_ITER), Alpha)
            ##
            ##print(DT_StepSize)
            if K_ITER%K_REFRESH == 0:
                dump_status(K_ITER, _T_, OBJ_BEST, DT_StepSize)
            if K_ITER%(10*K_REFRESH) == 0:
                print("==")
        tf=time.clock()
        print(".. END")
        print("total time",tf-t0)
        min_index, min_value = min(enumerate(OBJ_BEST), key=operator.itemgetter(1))

        print("The Best Objective :", min_value)
        b=min_index*145
        c=b+145
        print(DT_BEST[b:c])


################################################################
app=App()
Tasks = {
  #"run00":  (app.run00, "CL-IPSA Trial Solution 0"),
  #"run01": (app.run01, "CL-IPSA Trial Solution 1"),
  #"run02": (app.run02, "CL-IPSA Trial Solution 2"),
  #"run03": (app.run03, "CL-IPSA Trial Solution 3"),
  "ipsa": (app.ipsa, "CL-IPSA Trial Solution"),
}
if len(sys.argv) != 2 or sys.argv[1] not in Tasks:
    print("Usage: python cl_ipsa.py command")
    print("Available commands:")
    for cmd in Tasks:
        print("##*********************************")
        print("    ", cmd, ":", Tasks[cmd][1])
        print("##*********************************")
else:
    cmd = sys.argv[1]
    print(Tasks[cmd][1])
    Tasks[cmd][0]()